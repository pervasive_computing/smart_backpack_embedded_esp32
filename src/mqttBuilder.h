#include "mqtt.h"

/**
 * @brief utility class for making easier create instance of mqtt class
 * 
 */
class MqttBuilder
{
private:
    /* global vars */
    char* wifi_ssid;
    char* wifi_password;
    char* mqtt_broker;
    char* mqtt_topic;
    char* mqtt_username;
    char* mqtt_password;
    int mqtt_port;
    char* backpack_id;
    std::function<void (char *, uint8_t *, unsigned int)> mqtt_callback;
public:
    /**
     * @brief Construct a new MqttBuilder
     * 
     */
    MqttBuilder();
    ~MqttBuilder();
    /**
     * @brief Set the Wifi SSID
     * 
     * @param wifi_ssid a char* with the wifi SSID
     * @return MqttBuilder* 
     */
    MqttBuilder* setWifiSSID(char* wifi_ssid);
    /**
     * @brief Set the Wifi Password
     * 
     * @param wifi_password a char* with the wifi password
     * @return MqttBuilder* 
     */
    MqttBuilder* setWifiPassword(char* wifi_password);
    /**
     * @brief Set the Mqtt Broker
     * 
     * @param mqtt_broker a char* with the name of mqtt broker
     * @return MqttBuilder* 
     */
    MqttBuilder* setMqttBroker(char* mqtt_broker);
    /**
     * @brief Set the Mqtt Topic
     * 
     * @param mqtt_topic a char* with the name of mqtt topic
     * @return MqttBuilder* 
     */
    MqttBuilder* setMqttTopic(char* mqtt_topic);
    /**
     * @brief Set the Mqtt Username for login
     * 
     * @param mqtt_username a char* with the username needed for login to mqtt broker
     * @return MqttBuilder* 
     */
    MqttBuilder* setMqttUsername(char* mqtt_username);
    /**
     * @brief Set the Mqtt Password for login
     * 
     * @param mqtt_password a char* with the password needed for login to mqtt broker
     * @return MqttBuilder* 
     */
    MqttBuilder* setMqttPassword(char* mqtt_password);
    /**
     * @brief Set the Mqtt Port for connecting to broker
     * 
     * @param mqtt_port a char* with the port needed for connecting to mqtt broker
     * @return MqttBuilder* 
     */
    MqttBuilder* setMqttPort(int mqtt_port);
    /**
     * @brief Set the Backpack ID
     * 
     * @param backpack_id a char* with the ID of this backpack
     * @return MqttBuilder* 
     */
    MqttBuilder* setBackpackID(char* backpack_id);
    /**
     * @brief Set the callback for handle mqtt messages when arrive
     * 
     * @param callback standard function for handle arrived messages
     * @return MqttBuilder* 
     */
    MqttBuilder* setMqttCallBack(std::function<void (char *, uint8_t *, unsigned int)> callback);
    /**
     * @brief build Mqtt instance
     * 
     * @return Mqtt* ready to use
     */
    Mqtt* build();
};

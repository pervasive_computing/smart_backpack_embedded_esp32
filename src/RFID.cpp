#include "RFID.h"

RFID::RFID(int SS_pin, int RST_pin)
{
    this->rfid_tag_present_prev = false;
    this->rfid_tag_present = false;
    this->_rfid_error_counter = 0;
    this->_tag_found = false;
    this->lastTagIdRead = "";
    
    this->rfidController = new MFRC522(SS_pin, RST_pin);
    SPI.begin(); // init SPI bus
    this->rfidController->PCD_Init(); // init MFRC522
    //this->rfidController->PICC_DumpToSerial(&(this->rfidController->uid));
}

RFID::~RFID()
{
}

String RFID::readTag()
{
    String uid = "";

    for (int i = 0; i < this->rfidController->uid.size; i++) {
        if (this->rfidController->uid.uidByte[i] < 0x10) {
            uid = uid + " 0";
        } else {
            uid = uid + " ";
        }
        uid = uid + String(this->rfidController->uid.uidByte[i], HEX);
    }
    uid.remove(0, 1);
    return uid;
}

String RFID::getId()
{
    rfid_tag_present_prev = rfid_tag_present;

    _rfid_error_counter += 1;
    if(_rfid_error_counter > 2){
        _tag_found = false;
    }

    // Detect Tag without looking for collisions
    byte bufferATQA[2];
    byte bufferSize = sizeof(bufferATQA);

    // Reset baud rates
    this->rfidController->PCD_WriteRegister(this->rfidController->TxModeReg, 0x00);
    this->rfidController->PCD_WriteRegister(this->rfidController->RxModeReg, 0x00);
    // Reset ModWidthReg
    this->rfidController->PCD_WriteRegister(this->rfidController->ModWidthReg, 0x26);

    MFRC522::StatusCode result = this->rfidController->PICC_RequestA(bufferATQA, &bufferSize);

    if(result == this->rfidController->STATUS_OK){
        if (!this->rfidController->PICC_ReadCardSerial()) { //Since a PICC placed get Serial and continue   
            return this->lastTagIdRead;
        }
        _rfid_error_counter = 0;
        _tag_found = true;        
    }
    
    rfid_tag_present = _tag_found;
    
    // rising edge
    if (rfid_tag_present && !rfid_tag_present_prev){
        Serial.println("Tag found");
        this->lastTagIdRead = this->readTag();
    }
    
    // falling edge
    if (!rfid_tag_present && rfid_tag_present_prev){
        Serial.println("Tag gone");
        this->lastTagIdRead = "";
    }
    return this->lastTagIdRead;
}

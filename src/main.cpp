#include <Arduino.h>
#include "RFID.h"
#include "mqttBuilder.h"

#define SS1_PIN  5  // ESP32 pin GIOP5 
#define RST1_PIN 27 // ESP32 pin GIOP27

#define SS2_PIN  14  // ESP32 pin GIOP5 
#define RST2_PIN 13 // ESP32 pin GIOP27

/* data for connecting to WIFI */
#define WIFI_SSID "more"
#define WIFI_PASSWORD ""

/* data for connecting using MQTT */
#define MQTT_USERNAME ""
#define MQTT_PASSWORD ""
#define MQTT_BROKER "broker.emqx.io"
#define MQTT_TOPIC "pervasivecomputing/test"
#define MQTT_PORT 1883

/* ID of THIS BACKPACK */
#define BACKPACK_ID "1"


/* global vars */
RFID* rfid1;
RFID* rfid2;
Mqtt* mqtt;

void setup() {
    // Init Serial
    Serial.begin(115200);
    Serial.println("Tap RFID/NFC Tag on reader");

    // init RFID controller
    rfid1 = new RFID(SS1_PIN, RST1_PIN);
    rfid2 = new RFID(SS2_PIN, RST2_PIN);

    // init and setup Mqtt stuff
    MqttBuilder* mqttBuilder = new MqttBuilder();
    mqtt = mqttBuilder->setWifiSSID((char*) WIFI_SSID)
        ->setWifiPassword((char*) WIFI_PASSWORD)
        ->setMqttUsername((char*) MQTT_USERNAME)
        ->setMqttPassword((char*) MQTT_PASSWORD)
        ->setMqttBroker((char*) MQTT_BROKER)
        ->setMqttTopic((char*) MQTT_TOPIC)
        ->setMqttPort(MQTT_PORT)
        ->setBackpackID((char*) BACKPACK_ID)
        ->setMqttCallBack(
            [] (char *topic, byte *payload, unsigned int length) {
                // whole callback is just a print of Mqtt received messages
                Serial.print("Message arrived in topic: ");
                Serial.println(topic);
                Serial.print("Message:");
                for (int i = 0; i < length; i++) {
                    Serial.print((char) payload[i]);
                }
                Serial.println();
                Serial.println("-----------------------");
            }
        )
        ->build();
}

void loop() {
    // get ID of rfid tag
    String rfid1_id = rfid1->getId();
    String rfid2_id = rfid2->getId();
    // print rfid's ID
    Serial.println(rfid1_id);
    Serial.println(rfid2_id);
    
    // create a list for collect every rfid ID readed (in this case only one)
    std::list<String> detected_tags;
    detected_tags.push_back(rfid1_id);
    detected_tags.push_back(rfid2_id);

    // send all detected tags via Mqtt
    mqtt->send(detected_tags);
    
    // this will be repeated this every second
    delay(5000);
}

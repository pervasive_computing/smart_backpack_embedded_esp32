#include "mqttBuilder.h"

MqttBuilder::MqttBuilder()
{

}

MqttBuilder* MqttBuilder::setWifiSSID(char* wifi_ssid)
{
    this->wifi_ssid = wifi_ssid;
    return this;
}

MqttBuilder* MqttBuilder::setWifiPassword(char* wifi_password)
{
    this->wifi_password = wifi_password;
    return this;
}

MqttBuilder* MqttBuilder::setMqttBroker(char* mqtt_broker)
{
    this->mqtt_broker = mqtt_broker;
    return this;
}

MqttBuilder* MqttBuilder::setMqttTopic(char* mqtt_topic)
{
    this->mqtt_topic = mqtt_topic;
    return this;
}

MqttBuilder* MqttBuilder::setMqttUsername(char* mqtt_username)
{
    this->mqtt_username = mqtt_username;
    return this;
}

MqttBuilder* MqttBuilder::setMqttPassword(char* mqtt_password)
{
    this->mqtt_password = mqtt_password;
    return this;
}

MqttBuilder* MqttBuilder::setMqttPort(int mqtt_port)
{
    this->mqtt_port = mqtt_port;
    return this;
}

MqttBuilder* MqttBuilder::setBackpackID(char* backpack_id)
{
    this->backpack_id = backpack_id;
    return this;
}

MqttBuilder* MqttBuilder::setMqttCallBack(std::function<void (char *, uint8_t *, unsigned int)> callback)
{
    this->mqtt_callback = callback;
    return this;
}

Mqtt* MqttBuilder::build() 
{
    return new Mqtt(this->wifi_ssid, this->wifi_password, this->mqtt_broker, this->mqtt_topic, 
        this->mqtt_username, this->mqtt_password, this->mqtt_port, this->backpack_id, this->mqtt_callback);
}
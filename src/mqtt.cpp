#include "mqtt.h"

Mqtt::Mqtt(char* ssid, char* password, char* mqtt_broker, 
        char* mqtt_topic, char* mqtt_username, 
            char* mqtt_password, int mqtt_port, char* backpack_id,
                std::function<void (char *, uint8_t *, unsigned int)> mqtt_callback)
{
    this->mqtt_topic = mqtt_topic;
    this->backpack_id = backpack_id;
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.println("Connecting to WiFi..");
    }
    Serial.println("Connected to the WiFi network");
    //connecting to a mqtt broker
    this->espClient = new WiFiClient();
    this->client = new PubSubClient((*this->espClient));
    /* needed for increase size of mqtt messages */
    this->client->setBufferSize(THING_DESCRIPTION_BUFFER_SIZE);
    this->client->setServer(mqtt_broker, mqtt_port);
    this->client->setCallback(mqtt_callback);
    while (!this->client->connected()) {
        String client_id = "esp32-client-";
        client_id += String(WiFi.macAddress());
        Serial.printf("The client %s connects to the public mqtt broker\n", client_id.c_str());
        if (this->client->connect(client_id.c_str(), mqtt_username, mqtt_password)) {
            Serial.println("Public emqx mqtt broker connected");
        } else {
            Serial.print("Failed with state: ");
            Serial.print(this->client->state());
            delay(2000);
        }
    }

    this->client->subscribe(mqtt_topic);
}

void Mqtt::send(std::list<String> detected_tags)
{
    // create thing description
    DynamicJsonDocument thing_description = this->createThingDescription(this->backpack_id, detected_tags);
    // serialize JSON data into a char buffer
    char buffer[THING_DESCRIPTION_BUFFER_SIZE];
    serializeJson(thing_description, buffer);

    // print for debugging
    Serial.println("buffer: " + String(buffer));

    // send Thing Description
    this->client->publish(this->mqtt_topic, buffer);
    this->client->loop();
}

DynamicJsonDocument Mqtt::createThingDescription(char *backpack_id, std::list<String> detected_tags) {
    // create JSON data
    DynamicJsonDocument doc(THING_DESCRIPTION_BUFFER_SIZE);
    // set backpack_id
    
    doc[JSON_LD_BACKPACK_ID] = backpack_id;
    doc[JSON_LD_BACKPACK_DESCRIPTION] = "Backpack that contains a set of item used by paramedic for first aid";
    doc[JSON_LD_ACTIONS][JSON_LD_PUSH_ITEMS][JSON_LD_ACTION_DESCRIPTION] = "push items in backpack every seconds";

    // set detected_tags list
    JsonArray detected_tags_json_array = doc.createNestedArray(JSON_LD_ITEMS);
    // let's create a object for each rfid tag (item)
    for(const String tag_id : detected_tags) {
        // check if the item is actually detected or just an empty string
        if (!tag_id.isEmpty()) {
            // create a nested object (the item) like {"id": "99", "backpack_id": "7"}
            DynamicJsonDocument item(64);
            item[JSON_LD_ITEM_ID] = tag_id;
            item[JSON_LD_ITEM_BACKPACK_ID] = backpack_id;
            // add nested item to nested the array
            detected_tags_json_array.add(item);
        }
    }

    return doc;
}

Mqtt::~Mqtt()
{
}

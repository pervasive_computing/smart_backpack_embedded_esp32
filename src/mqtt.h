#include <WiFi.h>
#include <PubSubClient.h>
#include <list>
#include <ArduinoJson.h>

#define JSON_LD_CONTEXT "@context"
#define JSON_LD_TYPE "@type"
#define JSON_LD_BACKPACK_ID "id"
#define JSON_LD_TITLE "title"
#define JSON_LD_BACKPACK_DESCRIPTION "description"
#define JSON_LD_ACTIONS "actions"
#define JSON_LD_PUSH_ITEMS "push_items"
#define JSON_LD_ACTION_DESCRIPTION "description"
#define JSON_LD_ITEMS "items"
#define JSON_LD_ITEM_ID "id"
#define JSON_LD_ITEM_BACKPACK_ID "backpack_id"

/* set the thing description maximum size */
#define THING_DESCRIPTION_BUFFER_SIZE 1024

/**
 * @brief mqttBuilder for creating instance of this
 */
class Mqtt
{
private:
    /* global vars */
    WiFiClient* espClient;
    PubSubClient* client;
    char* mqtt_topic;
    char* backpack_id;
    /* private methods */
        /* specify what to do when mqtt message received */
    static void callback(char *topic, byte *payload, unsigned int length);
        /* utility method for creating thing description in json */
    DynamicJsonDocument createThingDescription(char *backpack_id, std::list<String> detected_tags);
public:
    /**
     * @brief Construct a new Mqtt object, use MqttBuilder!
     * 
     * @param ssid a char* with the wifi SSID
     * @param password a char* with the wifi password
     * @param mqtt_broker a char* with the name of mqtt broker
     * @param mqtt_topic a char* with the name of mqtt topic
     * @param mqtt_username a char* with the username needed for login to mqtt broker
     * @param mqtt_password a char* with the password needed for login to mqtt broker
     * @param mqtt_port a char* with the port needed for connecting to mqtt broker
     * @param backpack_id a char* with the ID of this backpack
     * @param mqtt_callback standard function for handle arrived messages
     */
    Mqtt(char* ssid, char* password, char* mqtt_broker, 
        char* mqtt_topic, char* mqtt_username, 
            char* mqtt_password, int mqtt_port, char* backpack_id,
                std::function<void (char *, uint8_t *, unsigned int)> mqtt_callback);
    ~Mqtt();
    /**
     * @brief send to mqtt server the items detected
     * 
     * @param detected_tags list of detected items
     */
    void send(std::list<String> detected_tags);
};

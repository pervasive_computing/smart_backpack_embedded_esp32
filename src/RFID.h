#include <MFRC522.h>
#include <SPI.h>

/**
 * @brief 
 * 
 */
class RFID
{
private:
    /* global vars */
    MFRC522* rfidController;
    bool rfid_tag_present_prev;
    bool rfid_tag_present;
    int _rfid_error_counter;
    bool _tag_found;
    String lastTagIdRead;
    /**
     * @brief check if there is a tag and get it
     * 
     * @return String with the tag data, empty string instead
     */
    String readTag();
public:
    /**
     * @brief Construct a new RFID
     * 
     * @param SS_pin in some mfrc522 controller could be as SDA
     * @param RST_pin reset pin
     */
    RFID(int SS_pin, int RST_pin);
    ~RFID();
    /**
     * @brief check if there is a tag and get its ID
     * 
     * @return String with the tag ID, empty string instead
     */
    String getId();
};